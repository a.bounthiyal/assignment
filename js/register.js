$(document).ready(function () {
    // click on button submit
    $("#submit").on('click', function () {
        // send ajax
        var email = $("#email").val();
        var password = $("#password").val();
        console.log(IsEmail(email));
        if(email== ''){
            $('#msg-email').html("Email id is required").show();
            return false;
        }
        if (IsEmail(email)== false) {
             $('#msg-email').html("Enter valid email").show();
            return false;
          }
        if(password== ''){
            $('#msg-pass').html("Password is required").show();
            return false;
        }
        if(password.length < 5){
            $('#msg-pass').html("Password should be five characters").show();
            return false;
        }
            var myData = {email: email, password: password};
            $.ajax({
                url: 'api/register.php',
                type: "POST",
                dataType: 'json',
                data: JSON.stringify(myData),
                success: function (data, status, xhr) {
                    console.log(data);
                    alert(data.message);
                },
                error: function (xhr, resp, text) {
                    console.log(xhr, resp, text);
                    alert(" Email id is exit.");
                }
            })
    });
});

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        return false;
    }else{
        return true;
    }
}