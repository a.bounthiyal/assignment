$(document).ready(function(){
    // click on button submit
    $("#login").on('click', function(){
        // send ajax
        var email = $("#email").val();
        var password = $("#password").val();
        if(email== ''){
            $('#msg-email').html("Email id is required").show();
            return false;
        }
        if (IsEmail(email)== false) {
            $('#msg-email').html("Enter valid email").show();
            return false;
        }
        if(password== ''){
            $('#msg-pass').html("Password is required").show();
            return false;
        }
        var myData = {email:email, password:password};
        console.log(myData);
        $.ajax({
            url: 'api/login.php',
            type : "POST",
            dataType : 'json',
            data : JSON.stringify(myData),
            success : function(data,status,xhr) {
               // console.log(data);
                alert(data.message);
                $('.card').show();
                $('#email-id').html(data.email);
                $('#jwt').html(data.jwt);
                $('#Expiry').html(data.expireAt);
            },
            error: function(xhr, resp, text) {
               // console.log(xhr, resp, text);
               alert(text +", "+ " Please enter Correct email-id and password.");
            }
        })
    });
});

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        return false;
    }else{
        return true;
    }
}